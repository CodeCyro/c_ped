C = {}

local onpermssit

Citizen.CreateThread(function() -- Thread for the loop

    C.katosteamid()

    while true do -- Loops while
        Citizen.Wait(150) 
    end -- End of while

end) -- End of thread loop


C.katosteamid = function() -- Steam identifer checking function

    onpermssit = nil

    TriggerServerEvent('C-ped:onpermssit') -- Server Event

    while (onpermssit == nil) do 
        Citizen.Wait(1) 
    end 

end


RegisterNetEvent('C-ped:katoSteamID') 
AddEventHandler('C-ped:katoSteamID', function(state) 

    onpermssit = state

end) 




RegisterCommand("ped", function(source, args, rawCommand) -- Register command
    if onpermssit then
        
        local ped = args[1]
        local pedinhash = GetHashKey(ped)

        if ped == nil then
            TriggerEvent('chat:addMessage', { -- Chat event
            color = { 255, 255, 255}, -- Title color
            multiline = true, -- multiline bool
            args = {"Ped System ", "Wrong PedModel"}
          })
            print("[ERROR] - Wrong ped")
        end  
        RequestModel(pedinhash)
    
        Citizen.CreateThread(function() 
            while not HasModelLoaded(pedinhash)
            do RequestModel(pedinhash)
                Citizen.Wait(0)
            end	
            print("[INFO] - Uusi pedi: " .. ped)
            SetPlayerModel(PlayerId(), pedinhash)

            TriggerEvent('chat:addMessage', { -- Chat event
            color = { 255, 255, 255}, -- Title color
            multiline = true, -- multiline bool
            args = {"Ped System ", "Your ped have been changed"}
          })
        end)
    else
        TriggerEvent('chat:addMessage', { -- Chat event
        color = { 255, 255, 255}, -- Title color
        multiline = true, -- multiline bool
        args = {"Ped System ", "You do not have permissions to use this!!"}
      })
    end
end) 

